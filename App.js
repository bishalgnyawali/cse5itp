/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
//import Register from './views/Register';
import React, {Fragment,useEffect,useState} from 'react';
import Welcome from './views/onboarding/Welcome';
import Register from './views/initial/Register';
import MindFulness from './views/mindfulness/MindFulness';
import InitialScreen from './views/initial/InitialScreen';
import LogIn from './views/initial/LogIn';
import Results from './views/onboarding/Results';
import TopTwo from './views/onboarding/TopTwo';
import HomePage from './views/dashboard/HomePage';
import MyProfile from './views/dashboard/MyProfile';
import Notifications from './views/notification/Notifications';
import Contact from './views/static/Contact';
import Gratitude from './views/Gratitude';
//import AsyncStorage from '@react-native-community/async-storage';

import Reflections from "./views/Reflections"

import SortableValues from './components/onboarding/SortableValues';
import { createAppContainer } from "react-navigation";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createStackNavigator } from "react-navigation-stack";
import {createSwitchNavigator} from "react-navigation";
import ReflectMood from './components/reflection/ReflectMood';
import ReflectActivity from './components/reflection/ReflectActivity';





const App = () => {

        const AppDrawerNavigator = createDrawerNavigator({
          
          HomePage:{screen:HomePage},
          MyProfile:{screen:MyProfile},
          Notifications:{screen:Notifications},
          Reflections: {screen: Reflections},
          ReflectMood: {screen: ReflectMood},
          ReflectAcitvity: {screen: ReflectActivity},
          Gratitude:{screen:Gratitude},
          MindFulness: {screen: MindFulness},
          Contact:{screen:Contact}
        },
        {
          drawerBackgroundColor:'#B6D349',

        });
        const WelcomeStackNavigator=createStackNavigator({
          Welcome: {screen: Welcome},
          SortableValues: {screen: SortableValues},
          Results:{screen:Results},
          TopTwo:{screen:TopTwo},
        },
        {
        defaultNavigationOptions: { 
        header: null,
        },
        });


        const AppStackNavigator = createStackNavigator({
          //InitialScreen:InitialScreen,
          Drawer: {screen: AppDrawerNavigator},
        },
        {
          defaultNavigationOptions: { 
            header: null,
          },
        });


        const AppSwitchNavigator = createSwitchNavigator(
        {
          LogIn: {screen: LogIn},
          //HomePage:{screen:HomePage},
          InitialScreen:{screen:InitialScreen},
          Register: {screen: Register},
          AppDrawerNavigator:AppDrawerNavigator,
          WelcomeStackNavigator:WelcomeStackNavigator,
          //LoginStackNavigator:LoginStackNavigator,
          AppStackNavigator:AppStackNavigator,
        },
        {
          initialRouteName:'InitialScreen',//check?'AppStackNavigator':'InitialScreen',
        }

        );

        const AppContainer = createAppContainer(AppSwitchNavigator)

          return (<AppContainer/>);
}

export default App;
