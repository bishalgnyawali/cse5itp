import { StyleSheet } from 'react-native';

const DefaultStyles = StyleSheet.create({
    container:{
        flex:1,
    },
    viewWrapper:{
        marginHorizontal:50,
        marginVertical: 30,
        flex:1,
        fontWeight:'bold',
    },
    logo:{
        height:50,
        width:50,
        marginRight:12//issue with ios on reflection page
    },
    textDark: {
        color: '#47525E'
    },
    textLight: {
        color: '#FFFFFF'
    },
    blue: {
        backgroundColor: '#00A6FF',
    },
    teal: {
        backgroundColor: '#009FAD',
    },
    green: {
        backgroundColor: '#B6D349',
    },
    icon: {
        height:24,
        width:24
    },
    inlineIcon: {
        fontSize: 30
    },
    veryHappy: {
        backgroundColor: '#D34970'
    },
    happy: {
        backgroundColor: '#AF4D87'
    },
    neutral: {
        backgroundColor: '#83538F'
    },
    sad: {
        backgroundColor: '#585487'
    },
    verySad: {
        backgroundColor: '#395072'
    },
    veryHappyText: {
        color: '#D34970'
    },
    happyText: {
        color: '#AF4D87'
    },
    neutralText: {
        color: '#83538F'
    },
    sadText: {
        color: '#585487'
    },
    verySadText: {
        color: '#395072'
    },
});

export default DefaultStyles;