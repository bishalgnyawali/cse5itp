import React from 'react'
import { View, Text,StyleSheet,Image } from 'react-native'


const ListItem = (props) => {
    return (
        <View style={styles.topViewContainer}>
            <Image style={{margin:5}} source={require('../assets/images/icons8-ios-filled-50.png')}/>
            <Text style={styles.titleSub}>{props.title} </Text>
            <Text>{props.counter}</Text>
        </View>
    )
}

export default ListItem;

const styles=StyleSheet.create({

    topViewContainer:{
        marginTop:30,
        flexDirection:'row',
        borderColor:'black',
        //borderBottomWidth:1,
        borderTopWidth:1
    },

    titleSub:{
        margin:0,
        fontSize:18
    }
});