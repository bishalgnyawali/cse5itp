import React,{useState,useEffect} from 'react'
import { 
    View, 
    Text,
    SafeAreaView,
    StyleSheet,
    Platform
} from 'react-native';
import Button from './common/Button';
import TextField from './common/TextField';
import Header from './common/Header';
import { SocialIcon } from 'react-native-elements'
import DefaultStyles from '../assets/Style';
import fetchPost from './api/fetchPost';

const RegistrationForm = (props) => {

   //console.warn(props);
    var [values,setValues]=useState({
        name:'',
        email:'',
        password:'',
        confirmPass:''
    });
    var [error,setError]=useState({
        errorName:'',
        errorEmail:'',
        errorPassword:'',
    });
    //var [pass,setPass]=useState(false);

    const validate=(text,type)=>{
        const regEmail=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        const regName=/^[a-zA-Z ]*$/;
        if(type==='name'){
            if(!regName.test(text)){
                setError({...error,errorName:'non alpha characters entered'});
            }
            else if(text.length===0){
                if(error.errorName==='')
                    setError({...error,errorName:'Name field cannot be Empty'});
            }
            else{
                setValues({...values,name:text});
                setError({...error,errorName:''});
            }
        }
        else if(type==='email'){
            if(!regEmail.test(text)){
                setError({...error,errorEmail:'Invalid Email Address Entered'});
            }
            else{
                setValues({...values,email:text});
                setError({...error,errorEmail:''});
            }
        }
        else if(type==='password'){
            if(text.length<=8){
                setError({...error,errorPassword:'min 8 Characters long'});
            }
            else{
                setValues({...values,password:text});
                setError({...error,errorPassword:''});
            }
        }
        else if(type==='confirmPass'){
            if(text!=values.password){
                setError({...error,errorPassword:'Password Do not match'});
            }
            else{
                setValues({...values,confirmPass:text});
                setError({...error,errorPassword:''});
            }
        }

    }
  
    const handleSubmit=async ()=>{
        
        //props.navigation.push('Welcome');
        props.navigation.navigate('Welcome');
        /* if(!values.name){
            //console.warn("empty names");
            await setError({...error,errorName:"Name cannot be empty"})
            //console.warn(error.errorName);
        }else{
            fetchPost(values,'http://localhost:8080/registerUser');
            props.navigation.navigate('Welcome');
        } */
 
        
    }


    return (
            
            <SafeAreaView style={{flex:1}}>
                
                <View style={styles.textInputContainer}> 
                <Header mainTitle="Register"/>
                    <TextField 
                        style={[styles.input,error.errorName==''?styles.valid:styles.invalid]}
                        placeholder={(error.errorName==='')? "Name":error.errorName}
                        //autoFocus={true}
                        onChangeText = {(nam)=>validate(nam,'name')}
                        placeholderTextColor = {(error.errorName==='')? "#fff":"#cb3436"}
                    />
                    <TextField 
                        style={[styles.input,error.errorEmail==''?styles.valid:styles.invalid]}
                        placeholder={(error.errorEmail==='')? "Email":error.errorEmail}
                        onChangeText = {(mail)=>validate(mail,'email')}
                        placeholderTextColor = {(error.errorEmail==='')? "#fff":"#cb3436"}
                    />
                    <TextField 
                        style={[styles.input,error.errorPassword==''?styles.valid:styles.invalid]}
                        placeholder={(error.errorPassword==='')? "Password":error.errorPassword}
                        secureTextEntry={true}
                        onChangeText = {(pass)=>validate(pass,'password')}
                        placeholderTextColor = {(error.errorPassword==='')? "#fff":"#cb3436"}
                        />
                    <TextField 
                        style={[styles.input,error.errorPassword==''?styles.valid:styles.invalid]}
                        placeholder={(error.errorPassword==='')? "Confirm Password":error.errorPassword}
                        secureTextEntry={true}
                        onChangeText = {(conpass)=>validate(conpass,'confirmPass')}
                        placeholderTextColor = {(error.errorPassword==='')?"#fff":"#cb3436"}
                    />
                    <Button onPress={()=>{handleSubmit(values.password,values.confirmPass)}} color={'green'} rounded={true} text={"Submit"}/>
                    <Text style={{fontSize: 18, color: '#fff', textAlign: 'center', fontWeight:'700', marginBottom: 18}} >Or</Text>
                    <Text style={{fontSize: 12, color: '#fff', textAlign: 'center', marginBottom: 24}} >
                        By continuing, you agree to MentallyFitApp's Terms and conditions and Privary Policy
                    </Text>
                    <SocialIcon
                        title='Sign In With Facebook'
                        button
                        type='facebook'
                        onPress={()=>{handleSubmit()}}
                        style={{
                            marginBottom: 24,  
                            marginHorizontal: 30,   
                            padding: 12, 
                            ...Platform.select({
                                    ios: {
                                        paddingBottom: 8,
                                    }
                
                            }),
                            height: 42,
                            alignItems: 'center',
                            alignSelf:'stretch',
                            justifyContent: 'center',
                            borderRadius: 20,
                        }}
                    />


                    {/* <Button onPress={()=>{handleSubmit()}} color={"blue"} rounded={true} text={"Sign up with Facebook"}/> */}
                </View>
            </SafeAreaView>
        
        
    )
}

export default RegistrationForm
const styles=StyleSheet.create({

    textInputContainer:{
        marginRight:20,
        marginTop:40,
        justifyContent:'flex-end',
        fontSize:30,
        //flex:1,
        padding:0,
        alignItems:"center"
    },
    input: {
        marginBottom:30,
        height: 48,
        color:'black',
        //borderColor: '#B9DD4B',
        borderWidth: 1,
        borderRadius:30,
        paddingHorizontal: 18,
        fontWeight: '700',
        fontSize: 16,
        alignSelf:'stretch',
        marginHorizontal: 30       
    },
    valid:{
        borderColor: '#B9DD4B',
    },
    invalid: {
        borderColor: '#c84f37',
    }
});
