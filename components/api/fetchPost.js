export default fetchPost=(values,url)=>{
    
    //console.log(values);
    //console.log(url);
    fetch(url,{
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({fullName:values.name,email:values.email,password:values.password,})
    }).then( (result) => {
      //console.warn(result.json());
      // Get the result
      // If we want text, call result.text()
      return result.json();
    }).then((data) => {
      // Do something with the result
      //console.warn(data);
    }).catch((error)=>{
       console.log(error);
    });
}

