import React, { Fragment } from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import DefaultStyles from '../../assets/Style';

const Button = (props) => {
    return (
        <Fragment>
            <TouchableOpacity 
                onPress={props.onPress} 
                style={[styles.btn, color(props.color), props.rounded ? styles.rounded: '' ]}>
                <Text style={[ styles.btnText, props.color == 'white' ? DefaultStyles.textDark: DefaultStyles.textLight]}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </Fragment>
    );
}

export default Button

Button.defaultProps = {
    rounded: false,
    color: 'blue'
}

color = (color) => {
    switch (color.toLowerCase()) {
        case 'blue':
            return DefaultStyles.blue;
        case 'green':
            return DefaultStyles.green;
        case 'teal':
            return DefaultStyles.teal;
        case 'white':
            return styles.white;
        default:
            return styles.white; 
    }
}

const styles = StyleSheet.create({
    btn: {
        marginBottom: 24,  
        marginHorizontal: 30,   
        padding: 12,   
        height: 42,
        alignItems: 'center',
        alignSelf:'stretch',
        justifyContent: 'center',
        borderRadius: 6,
    },
    rounded:{
        borderRadius: 30,
    },
    white: {
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderColor: '#47525E',
    },
    btnText: {
        textAlign: 'center',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: '700',
        textTransform: 'uppercase',
        letterSpacing: 1,
        color: '#fff'
    },
    btnIcon: {
        resizeMode: 'contain',
        width: 25,
        height: 25,
    }
});
