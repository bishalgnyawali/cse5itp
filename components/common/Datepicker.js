import React, { useState } from 'react'
import DatePicker from 'react-native-datepicker'
import Icon from 'react-native-vector-icons/Feather'
import DefaultStyles from '../../assets/Style';
import moment from 'moment';

const today = new Date();
 
export default Datepicker = (props) => {
    const [date, setDate] = useState(today);
    
    return (
      <DatePicker
        style={{width: 200,borderWidth:1,borderColor:'#395072'}}
        date={date}
        mode="datetime"
        placeholder="select date"
        format="LL"
        minDate={props.minDate}
        maxDate={props.maxDate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
            dateIcon: {position:'absolute',left:0,width:30},
            dateInput: {borderWidth:0, marginLeft: 12}
        }}
        iconComponent={<Icon style={{color: '#000', fontSize: 18, position:'absolute',left:12}} name="calendar" />}
        onDateChange={(date) => setDate(date)}
      />
    )
}
Datepicker.defaultProps = {
    minDate: moment(today).subtract(1, 'year'),
    maxDate: today
}