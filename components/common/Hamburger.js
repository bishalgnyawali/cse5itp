import React from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import DefaultStyles from '../../assets/Style';

export default Hamburger = (props) => {
    let parent = props.navigation.dangerouslyGetParent();
    let isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
    return <View style={{paddingHorizontal: 20, paddingVertical: 10}}>
            <TouchableOpacity  onPress={isDrawerOpen ? null : () => props.navigation.openDrawer()}>
                <Image style={DefaultStyles.icon} source = {require('../../assets/images/menu.png')} />
            </TouchableOpacity>
        </View>;
}