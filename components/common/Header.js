import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import DefaultStyles from '../../assets/Style';
import Icon from "react-native-vector-icons/Feather";

const Header = (props) => {
    getDrawer = () => {
        if (props.navigation){
            const parent = props.navigation.dangerouslyGetParent();
            const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
            return <View style={{paddingHorizontal: 20, paddingVertical: 10}}>
                        <TouchableOpacity  onPress={isDrawerOpen ? null : () => props.navigation.openDrawer()}>
                            <Icon style={[DefaultStyles.inlineIcon]} name="menu" />
                        </TouchableOpacity>
                    </View>;
        } else {
            return null;
        }
    }
    return (
        <View>
            {this.getDrawer()}
            <View style= {[{ justifyContent:'center', alignSelf:'center' }]}>
                <Text style={[styles.titleMain, DefaultStyles.textDark]}>{props.mainTitle}</Text>
                <Text style={styles.titleSub}>{props.subTitle}</Text>
            </View>
        </View>        
    )
}

export default Header

const styles=StyleSheet.create({
    titleMain:{
        alignSelf:'center',
        //fontFamily:'Kanit',
        fontWeight:"bold",
        fontSize:35,
        color:'black'
    },
    titleSub:{
        alignSelf:'center',
        margin:0,
        fontSize:18,
        paddingTop:5
    }
});