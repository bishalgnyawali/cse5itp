import React from 'react'
import { 
    View, 
    Text,
    StyleSheet,
    Image,
    TouchableOpacity

} from 'react-native';
import DefaultStyles from '../../assets/Style';
//import console = require('console');


export default ImageDescription=(props)=>{
    return (
                <View style={styles.imageWrapper}>
                    <TouchableOpacity onPress={()=>{console.log(props.title+" pressed")}}>
                        <Image style={styles.logo} source={props.source}/>
                    </TouchableOpacity>
                    <Text style={styles.text}>{props.title}</Text>
                    
                </View>
    )

}

const styles=StyleSheet.create({
    logo:{
        height:50,
        width:50,
        //margin:5,
        marginRight:12,
        resizeMode:'cover',
        //marginRight:12//issue with ios on reflection page
    },
    text:{
        fontWeight:'bold',
        shadowColor:'black',
        fontSize:15,
        //justifyContent:'center',
        padding:15
    },
    imageWrapper:{
        margin:0,
        justifyContent:'space-around',

        //flex:1,
        
    },
    safeAreaViewWrapper:{
        flex:1,
    },
    imageWrapper:
    {
        
    }
});


