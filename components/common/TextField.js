import React from 'react'
import {TextInput} from 'react-native'

const TextField = (props) => {
    return (
        <TextInput 
            style = {props.style}
            underlineColorAndroid = "transparent"
            autoFocus={props.autoFocus}
            placeholder = {props.placeholder}
            placeholderTextColor = {props.placeholderTextColor}
            autoCapitalize = "none"
            autoCorrect={false}
            value={props.value}
            keyboardType={props.keyboardType}
            secureTextEntry={props.secureTextEntry}
            onChangeText = {props.onChangeText}
            editable
            />
    )
}


export default TextField


