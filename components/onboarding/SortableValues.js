import React,{useState} from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    Dimensions,
    Platform,
} from 'react-native';
import Button from '../common/Button';
import SortableList from 'react-native-sortable-list';
import Row from '../../views/onboarding/Row';
import {data1,data2,data3,data4,data5} from '../../objects/Data'

import Header from '../common/Header'
import DefaultStyles from '../../assets/Style';
const window = Dimensions.get('window');

let sortValue=[]
let result=[0,0,0,0,0]
var datam=[data1,data2,data3,data4,data5];
//console.warn(data);
export default SortableValues =(props)=> {
    const [index,setIndex]=useState(0);
    _renderRow = ({data, active}) => {
        return <Row data={data} active={active} />
    }

    return (
        <SafeAreaView style={[styles.container, DefaultStyles.green]}>
            <View style={styles.headerWrapper}>
                <Header mainTitle="Your Values" subTitle="Think about what is most important to you, and order the items from most to least important"/>
            </View>

            <SortableList
                style={styles.list}
                contentContainerStyle={styles.contentContainer}
                data={datam[index]}
                renderRow={_renderRow} 
                scrollEnabled={false}
                //onChangeOrder={(nextOrder) => console.warn(nextOrder)}
                onReleaseRow={(key,currentOrder)=>{
                    sortValue=currentOrder;
                    //console.warn(sortValue)
                }}
                />
            <Button
            onPress={()=>{
                    for(i=0;i<5;i++){
                            result[i] =result[i]+ sortValue.indexOf(i.toString());
                            //console.warn('index of'+i+"="+result[i]);
                        }
                    if(index<4){
                        
                        setIndex(index+1)
                        //console.warn("sorted list"+sortValue)
                        //console.warn('result'+result)
                    }
                    else{
                        //console.warn("sorted list"+sortValue)
                        console.warn('result'+result)
                        //props.navigation.navigate('LogIn');
                        props.navigation.navigate('Results', {result})
                    }
                }} 
            color={'teal'} 
            text={'Next'}/>
        
        
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        //flexWrap:'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        //marginBottom:3,
        ...Platform.select({
            ios: {
                paddingTop: 20,
                
            },
        }),
    },
    headerWrapper:{
        // shadowColor:'black',
        // borderWidth:1 ,
        // shadowRadius: 2,
        // elevation: 1,
        // backgroundColor:'#ececec',
        // marginRight:30,
        // shadowOffset: { width: 0, height: 2 },
        // padding:5,
        // marginLeft:30, 
        // shadowOpacity: 1.0,
        justifyContent:'center',
        alignContent:'center',
        alignSelf:'stretch',
        marginHorizontal: 30,
    },
    list: {
        flex: 1,
        //flexWrap:'wrap'
       
    },
    contentContainer: {
        width: window.width,
        height:window.height-70,
        //flexWrap:'wrap',
        marginTop:20,
        ...Platform.select({
            ios: {
                paddingHorizontal: 30,
            },
            android: {
                paddingHorizontal: 0,
            }
        })
    },
});
