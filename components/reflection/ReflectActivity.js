import React, {useState} from 'react'
import { 
    View, 
    TextInput,
    SafeAreaView,
    StyleSheet,
    ScrollView,
    Dimensions
}from 'react-native';
import Header from '../common/Header';
import {CheckBox,SearchBar} from 'react-native-elements';
import Button from '../common/Button';
//import Icon from 'react-native-vector-icons/FontAwesome';

/*KeyBoard Avoiding View with margin for header should be done*/



export default  ReflectActivity=()=>{
    const [check,setCheck]=useState({
        work:false,
        goneFishing:false,
        gym:false,
        walk:false,
    });
    return (
        <SafeAreaView style={styles.safeAreaViewWrapper}>
            <Header mainTitle="Reflection" subTitle="What have you been Upto?
                List of Activities across all Values"></Header>
            <SearchBar
                placeholder=" Search"
                round={true}
                containerStyle={{backgroundColor: '#B6D349',margin:5,marginTop:10, borderBottomWidth:0,borderTopWidth:0,borderColor:'#ccc'}}
                //onChangeText={this.updateSearch}
                value={'hello'}
                //inputStyle={{backgroundColor: '#ccc',margin:0}}
                inputContainerStyle={{backgroundColor: '#ccc',height:42,marginBottom:0,borderWidth:0,borderColor:'#ccc'}}
            />
            <ScrollView style={styles.viewWrapper}>
                <CheckBox
                    title='Work'
                    checked={check.work}
                    onPress={()=>setCheck({...check,work:!check.work})}
                    containerStyle={styles.iconContainer}
                    uncheckedColor="black"
                />
                <CheckBox
                    title='Gone Fishing'
                    checked={check.goneFishing}
                    onPress={()=>setCheck({...check,goneFishing:!check.goneFishing})}
                    containerStyle={styles.iconContainer}
                    uncheckedColor="black"
                />
                <CheckBox
                    title='Gym'
                    checked={check.gym}
                    onPress={()=>setCheck({...check,gym:!check.gym})}
                    containerStyle={styles.iconContainer}
                    uncheckedColor="black"
                />
                <CheckBox
                    title='Walk'
                    checked={check.walk}
                    onPress={()=>setCheck({...check,walk:!check.walk})}
                    containerStyle={styles.iconContainer}
                    uncheckedColor="black"
                />
                <TextInput 
                    style={[styles.text,styles.multiText]}
                    editable
                    multiline={true}
                    maxLength={400}
                    numberOfLines={10}
                    borderBottomColor={'green'}
                />
            </ScrollView>
            <Button color={'blue'} text={'Done'} />
        </SafeAreaView>
    )

}

const styles=StyleSheet.create({
    
    text:{
        fontWeight:'bold',
        shadowColor:'black',
        //justifyContent:'center',
        marginTop:8,
    },
    viewWrapper:{
        margin:8,
        flex:1,
        fontWeight:'bold',
    },
    safeAreaViewWrapper:{
        flex:1,
        backgroundColor: '#B6D349',
        paddingLeft:12,
        paddingRight:12
    },
    iconContainer:{
        backgroundColor:"#B6D349",
        borderWidth:0,
        marginLeft:8,
        marginRight:12,
    },
    multiText:{
        height:Dimensions.get('window').height*0.18,
        textAlignVertical:'top',
        width:Dimensions.get('window').width-10,
        padding:5,
        paddingVertical:8,
        marginRight:5,
        backgroundColor:'#ccc',
    }
});


