import React, {useState, useEffect, useRef} from 'react'
import {View,Text,SafeAreaView,StyleSheet} from 'react-native';
import Header from '../common/Header';
import Emoji from "react-native-vector-icons/FontAwesome5"

import DefaultStyles from '../../assets/Style';
import Datepicker from '../common/Datepicker';
import Button from '../common/Button';

export default  ReflectMood = (props) => {
    const [date, setDate] = useState(new Date());

    return (
        <SafeAreaView style={[DefaultStyles.container, DefaultStyles.green]}>
            <Header navigation={props.navigation} mainTitle="Reflection"></Header>
            <View style={DefaultStyles.viewWrapper}>
                    <View style={{display: 'flex', alignItems: 'center'}}>
                        <Datepicker / >
                        <Text style={{fontSize:28, textAlign: 'center', marginTop:80, marginBottom:12}}>How do you feel today?</Text>
                    </View>
                    <View style={styles.emojiWrapper}>
                        <View style={{display:'flex',flexDirection: 'column',alignItems:'center'}}>
                            <Emoji onPress={() => console.warn('emoji pressed')} style={[styles.emoji, DefaultStyles.veryHappyText]} name="grin-beam" />
                            <Text>Rad</Text>
                        </View>
                        <View style={{display:'flex',flexDirection: 'column',alignItems:'center'}}>
                            <Emoji onPress={() => console.warn('emoji pressed')} style={[styles.emoji, DefaultStyles.happyText]} name="smile" />
                            <Text>Happy</Text>
                        </View>
                        <View style={{display:'flex',flexDirection: 'column',alignItems:'center'}}>
                            <Emoji onPress={() => console.warn('emoji pressed')} style={[styles.emoji, DefaultStyles.neutralText]} name="meh" />
                            <Text>Meh</Text>
                        </View>
                        <View style={{display:'flex',flexDirection: 'column',alignItems:'center'}}>
                            <Emoji onPress={() => console.warn('emoji pressed')} style={[styles.emoji, DefaultStyles.sadText]} name="frown" />
                            <Text>Sad</Text>
                        </View>
                        <View style={{display:'flex',flexDirection: 'column',alignItems:'center'}}>
                            <Emoji onPress={() => console.warn('emoji pressed')} style={[styles.emoji, DefaultStyles.verySadText]} name="sad-tear" />
                            <Text>Awful</Text>
                        </View>
                    </View>
                    <Button color={'teal'} rounded={true} text="Next" onPress={props.navigation.navigate('ReflectActivity')} />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    emojiWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginBottom:50,
    },
    emoji: {
        fontSize: 45,
        margin: 5,
    }
});


