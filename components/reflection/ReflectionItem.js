import React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import Emoji from "react-native-vector-icons/FontAwesome5";
import moment from "moment";
import DefaultStyles from "../../assets/Style";

export default ReflectionItem = (props) => {
    emoji = () => {
        switch (props.mood) {
            case 'very happy':
                return {icon: 'grin-beam' ,color: DefaultStyles.veryHappyText.color}
            case 'happy':
                return {icon: 'smile' ,color: DefaultStyles.happyText.color}
            case 'meh':
                return {icon: 'meh' ,color: DefaultStyles.neutralText.color}
            case 'sad':
                return {icon: 'frown' ,color: DefaultStyles.sadText.color}
            case 'very sad':
                return {icon: 'sad-tear' ,color: DefaultStyles.verySadText.color}
            default:
                return {icon: 'meh', color: '#fff'}
        }
    }
    return (
        <View style={[styles.reflectionWrapper, {borderColor: emoji().color,backgroundColor:'rgba(255,255,255,0.1)'}]}>
            <View>
                <Emoji name={emoji().icon} style={[{fontSize:35 ,color:emoji().color, marginRight:12}]} />
            </View>
            <View style={{paddingRight:50}}>
                <Text style={{fontSize:16}}>{moment(props.date).calendar()}</Text>
                <Text style={{marginTop:8}}>
                    {props.activities
                        .map((activity, index) => <Text key={index}>{activity}</Text>)
                        .reduce((prev, curr) => [prev, ', ', curr])}
                </Text>
            </View>
        </View>
    );
}
ReflectionItem.defaultProps = {
    mood: 'meh',
    date: new Date(),
    activities: []
}
const styles = StyleSheet.create({
    reflectionWrapper: {
        display: 'flex', 
        flexDirection: 'row', 
        alignItems: 'center',
        borderWidth: 1,
        borderLeftWidth: 5, 
        padding: 8
    }
})