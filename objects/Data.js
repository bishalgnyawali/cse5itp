export const data1 = {
    0: {
        image: 'https://placekitten.com/200/240',
        text: 'Friends, Family, Cooperation',
    },
    1: {
        image: 'https://placekitten.com/200/201',
        text: 'Achievement, Goals, Purpose',
    },
    2: {
        image: 'https://placekitten.com/200/202',
        text: 'Choices, Self Reliance, Free Will',
    },
    3: {
        image: 'https://placekitten.com/200/203',
        text: 'Play, Pleasure, Enjoyment',
    },
    4: {
        image: 'https://placekitten.com/200/204',
        text: 'Safety, Comfort, Shelter',
    }
};

export const data2 = {
    0: {
        image: 'https://placekitten.com/200/240',
        text: 'Relationships,Communication,\nCloseness',
    },
    1: {
        image: 'https://placekitten.com/200/201',
        text: 'Growth, Strength,Control',
    },
    2: {
        image: 'https://placekitten.com/200/202',
        text: 'Independence, Space \nSelecting',
    },
    3: {
        image: 'https://placekitten.com/200/203',
        text: 'Laughter, Joy, Amusement',
    },
    4: {
        image: 'https://placekitten.com/200/204',
        text: 'Neatness,Order,Procedures',
    }
};

export const data3 = {
    0: {
        image: 'https://placekitten.com/200/240',
        text: 'Feelings,Sympathy,Helping',
    },
    1: {
        image: 'https://placekitten.com/200/201',
        text: 'Competition,High Standards,\n Learning',
    },
    2: {
        image: 'https://placekitten.com/200/202',
        text: 'Novelty, Change Freedom',
    },
    3: {
        image: 'https://placekitten.com/200/203',
        text: 'Games,Humour,Fun',
    },
    4: {
        image: 'https://placekitten.com/200/204',
        text: 'Health,Caution,Security',
    }
};

export const data4 = {
    0: {
        image: 'https://placekitten.com/200/240',
        text: 'Pets, kindness, warmth',
    },
    1: {
        image: 'https://placekitten.com/200/201',
        text: 'recognition being heard \nwinning',
    },
    2: {
        image: 'https://placekitten.com/200/202',
        text: 'adventure risk outdoors',
    },
    3: {
        image: 'https://placekitten.com/200/203',
        text: 'spontaneity excitement \ncelebration',
    },
    4: {
        image: 'https://placekitten.com/200/204',
        text: 'stability tradition investment',
    }
};

export const data5 = {
    0: {
        image: 'https://placekitten.com/200/240',
        text: 'understanding generosity \nnurturing',
    },
    1: {
        image: 'https://placekitten.com/200/201',
        text: 'challenge skillfulness work',
    },
    2: {
        image: 'https://placekitten.com/200/202',
        text: 'flexibility travel liberty',
    },
    3: {
        image: 'https://placekitten.com/200/203',
        text: 'with innovation variety',
    },
    4: {
        image: 'https://placekitten.com/200/204',
        text: 'stability tradition investments',
    }
};