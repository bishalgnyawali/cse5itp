    export default ImageIcoObj= [
    {
        image:'(\'../assets/emoticons/1.png\')',
        title: 'Rad',
    },
    {
        image:'(\'../assets/emoticons/2.png\')',
        title: 'Good',
    },
    {
        image: '(\'../assets/emoticons/3.png\')',
        title: 'Normal',
    },
    {
        image: '(\'../assets/emoticons/4.png\')',
        title: 'Sad',
    },
    {
        image:'(\'../assets/emoticons/5.png\')',
        title: 'Awful',
    }
]