export const reflections = [
    {
        mood:'sad',
        date: '2019-08-19',
        description: 'Lorem ipsum draco dormien',
        activties: ['hiking','reading a book']
    },
    {
        mood:'happy',
        date: '2019-09-17',
        description: 'Lorem ipsum draco dormien',
        activties: ['hiking','reading a book','trying out a new restaurant', 'swimming']
    },
    {
        mood:'meh',
        date: '2019-09-18',
        description: 'Lorem ipsum draco dormien',
        activties: ['hiking','reading a book']
    },
    {
        mood:'very happy',
        date: '2019-09-19',
        description: 'Lorem ipsum draco dormien',
        activties: ['hiking','reading a book','trying out a new restaurant', 'swimming']
    },
    {
        mood:'very sad',
        date: '2019-09-20',
        description: 'Lorem ipsum draco dormien',
        activties: ['hiking','reading a book']
    }
];