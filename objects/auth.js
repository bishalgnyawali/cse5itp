import AsyncStorage from '@react-native-community/async-storage';
export const USER_KEY = "auth-demo-key";

export const onSignIn = async() => await AsyncStorage.setItem('USER_KEY', "true");

export const onSignOut = async() => await AsyncStorage.removeItem('USER_KEY');

export async function isSignedIn() {

        const res = await AsyncStorage.getItem('USER_KEY');
        await console.log(res);
        return (res);
    
};