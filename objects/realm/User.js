export default {
    name: "User",
    properties: {
      imageUri: "string?",
      tags: "string?"
    }
};
  