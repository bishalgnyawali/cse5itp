import React from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    Image,
    TouchableOpacity,
    Dimensions,
    TouchableWithoutFeedback,
    Modal,
    PermissionsAndroid,
    TextInput,
    ScrollView
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-picker';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode';
import realm from '../objects/realm/index';


const { width, height } = Dimensions.get('window');

class Gratitude extends React.Component {

    constructor(props) {
        super(props);
        this.user = realm.objects('User');
        this.realmListner = () => this.forceUpdate();
        realm.addListener('change', this.realmListner);
        this.state = {
            filePathURI: "",
            inputTagsModal: false,
            tags: "",
            imageExists: false,
            currentImageIndex: -1,
            currentTags: "",
            categorySelected: "",
            currentTagIndex: -1,
            loader: false
        };
    }

    componentDidMount() {
        var flag = false;
        this.user.map((image, index)=> {
            if(image.imageUri !== "") {
                flag = true
            }
        })
        if(this.user.length !== 0 && flag) {
            this.setState({
                imageExists: true,
                currentImageIndex: 0,
                currentTags: this.user[0].tags
            });
        }
    }

    closeModalAndSelectImage() {
        
        this.selectImage()
    }

    selectImage = () => {
        const options = {
            title: 'Select Photo',
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
          
            if (response.didCancel) {
            } else if (response.error) {
            } else {
                const source = { uri: response.uri };
                realm.write(() => {
                    realm.create('User', {
                        imageUri: response.uri,
                        tags: ""
                    });
                });
                this.setState({
                    inputTagsModal: false,
                    imageExists: true
                });
            }
        });
    }

    addToLocalSTorage() {
        realm.write(() => {
            realm.create('User', {
              imageUri: "",
              tags: this.state.tags
            });
        });
        
        this.setState({
            tags: "",
            categorySelected: "",
            inputTagsModal: false
        });

    }

    selectCurrentImage(idx) {
        this.setState({
            currentImageIndex: idx
        });
    }

    deleteSelectedImage(idx) {
        var count = 0;
        this.user.map((image, index)=> {
            if(image.imageUri !== "") {
                count += 1;
            }
        });

        if(count == 1) {
            realm.write(()=> {
                realm.delete(this.user[idx]);
            });
            this.setState({
                imageExists: false
            });
        }

        else {
            realm.write(()=> {
                realm.delete(this.user[idx]);
            })
        }
    }

    selectTag(idx) {
        this.setState({
            currentTagIndex: idx
        });
    }

    renderAllLocalImages() {
        let { currentImageIndex } = this.state;
        return this.user.map((image, index)=> {
            if(image.imageUri === "") {
                return null
            }
            return (
                <TouchableOpacity key= {index} onPress= {()=> this.selectCurrentImage(index)}>
                    <Image key= {index} source= {{ uri: image.imageUri }} resizeMode= {ImageResizeMode.contain} style= { index == currentImageIndex ? styles.imageActiveStyle : styles.imageStyle } />
                    {
                        currentImageIndex == index ? (
                            <TouchableOpacity
                                key= {index}
                                onPress= {()=> this.deleteSelectedImage(index)}
                                style= {styles.deleteViewStyle}>
                                <Feather name= "x" color= "black" size= {20} style= {{alignSelf: 'center'}} />
                            </TouchableOpacity>
                        ) : null
                    }
                </TouchableOpacity>
            )
        })
    }

    renderTagsList() {
        const { currentTagIndex } = this.state;
        return this.user.map((tag, index)=> {
            if(tag.tags == "") {
                return null
            }
            return (
                <View key= {index + Math.random()} style= {{flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center'}}>
                    <Text onPress= {()=> this.selectTag(index)} key= {index + Math.random()} style= {styles.scrollListTags}>{tag.tags}</Text>
                    {
                        currentTagIndex == index ? (
                            <TouchableOpacity
                                key= {index + Math.random()}
                                onPress= {()=> this.deleteSelectedImage(index)}
                                style= {styles.deleteTagStyle}>
                                <Feather name= "x" color= "black" size= {20} style= {{alignSelf: 'center'}} />
                            </TouchableOpacity>
                        ) : null
                    }
                    </View>
                )
        });
    }

    render() {
        return (
                <View style={{ flex: 1, height, backgroundColor: '#B6D349' }}>
                    <SafeAreaView>
                    <Feather 
                        onPress= {()=> this.props.navigation.toggleDrawer()}
                        name="menu" color="black" style={styles.menuStyle} size={30} />
                    
                    <View style= {styles.textTitle}>
                        <Text style= {styles.text}>What am I grateful for ?</Text>
                    </View>

                    <Text style= {[styles.text, {marginLeft: 25, marginTop: 20}]}>Photos</Text>
                    <ScrollView horizontal contentContainerStyle= {styles.horizontalScroll} showsHorizontalScrollIndicator= {false}>
                        {
                            this.state.imageExists ? this.renderAllLocalImages() : (
                                <>
                                    <Image source= {require('../assets/images/puppy.png')} resizeMode= {ImageResizeMode.contain} style= {styles.imageStyle} />
                                    <Image source= {require('../assets/images/glassesBeach.png')} resizeMode= {ImageResizeMode.contain} style= {styles.imageStyle} />
                                    <Image source= {require('../assets/images/car.png')} resizeMode= {ImageResizeMode.contain} style= {styles.imageStyle} />
                                </>
                            )
                        }
                    </ScrollView>

                    <View style= {styles.subViewStyle}>
                        <Text style= {[styles.text, {marginLeft: 5, fontSize: 24}]}>My List</Text>
                        <ScrollView contentContainerStyle= {styles.horizontalScroll} showsVerticalScrollIndicator= {false}>
                            {
                                this.renderTagsList()
                            }
                        </ScrollView>
                    </View>



                    <Modal
                        animationType= "fade"
                        transparent= {true}
                        onRequestClose={() => this.setState({ inputTagsModal: false, categorySelected: "" })}
                        visible={this.state.inputTagsModal}
                    >
                      <TouchableOpacity
                        onPress={() => this.setState({ inputTagsModal: false, categorySelected: "" })}
                        activeOpacity={1}
                      >
                        <View style={{ flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', width, height }}>
                          <View style={{ borderRadius: 10, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', padding: 10, width: '100%', height, backgroundColor: "#2f364010" }}>
                            <TouchableWithoutFeedback>
                              <View style={{ borderRadius: width / 30, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingVertical: 20, paddingHorizontal: 8, width: '80%', height: (height / 2.5), backgroundColor: "#fff", elevation: 4 }}>

                                {
                                    this.state.categorySelected == "tags" ? (
                                        <>
                                        <Text style= {styles.text}>Enter your Note</Text>
                                        <TextInput
                                            value={this.state.tags}
                                            onChangeText={(text) => this.setState({ tags: text })}
                                            placeholderTextColor="#95a5a6"
                                            style={styles.textInputStyle}
                                            placeholder= "Enter here"
                                        />

                                        <TouchableOpacity
                                            onPress= {()=> this.addToLocalSTorage()}
                                            style= {{width: '70%', padding: 12, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', borderRadius: width / 10, marginTop: width / 10}}>
                                            <Text style= {[styles.text, {color: 'white'}]}>Add</Text>
                                        </TouchableOpacity>
                                        </>
                                    ) : (
                                        <>
                                        <Text style= {styles.text}>Select Any:</Text>
                                        <View style= {{flexDirection: 'row', width: '100%', marginTop: 20, justifyContent: 'space-around', alignItems: 'center'}}>
                                            <TouchableOpacity  
                                                style= {{width: '40%', padding: 12, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', borderRadius: width / 80 }}
                                                onPress= {()=> this.closeModalAndSelectImage()}>
                                                <Text style= {[styles.text, {color: 'white'}]}>Image</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style= {{width: '40%', padding: 12, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', borderRadius: width / 80 }}
                                                onPress= {()=> this.setState({ categorySelected: "tags" })}>
                                                <Text style= {[styles.text, {color: 'white'}]}>Note</Text>
                                            </TouchableOpacity>
                                        </View>
                                        </>
                                    )
                                }

                              </View>
                            </TouchableWithoutFeedback>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </Modal>

                    <TouchableOpacity style= {styles.floatingActionButton}
                        onPress= {()=> this.setState({ inputTagsModal: true })}>
                            <Feather name= "plus" color= "white" size= {width / 16} style= {{alignSelf: 'center'}} />
                    </TouchableOpacity>


                </SafeAreaView>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    imageStyle: {
        width: width / 3,
        height: width / 3,
        marginRight: 10
    },
    imageActiveStyle: {
        width: width / 3,
        height: width / 3,
        marginRight: 10,
    },
    floatingActionButton: {
        position: 'absolute',
        right: 20,
        top: height - (width / 7) - 20,
        zIndex: 100,
        width: width / 7,
        height: width / 7,
        borderRadius: width / 14,
        backgroundColor: "#00A6FF",
        justifyContent: 'center',
        alignItems: 'center'
    },
    deleteViewStyle: {
        width: 25, 
        height: 25, 
        position: 'absolute', 
        top: 4, 
        left: 4, 
        backgroundColor: 'white', 
        borderRadius: 50, 
        zIndex: 1000, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    deleteTagStyle: {
        width: 25, 
        height: 25, 
        backgroundColor: 'white', 
        marginTop: width / 15,
        borderRadius: 50,
        marginRight: 20,
        justifyContent: 'center', 
        zIndex: 100,
        alignItems: 'center'
    },
    textInputStyle: {
        width: '85%',
        marginTop: 30,
        paddingHorizontal: 18,
        fontSize: width / (width / 12) + 2,
        paddingVertical: height / 40,
        borderWidth: 0.5,
        borderRadius: 8,
        borderColor: '#d1ccc0',
    },
    horizontalScroll: {
        marginLeft: 20,
    },
    menuStyle: { 
        alignSelf: 'flex-start', 
        marginLeft: 20, 
        marginTop: 20 
    },
    text: {
        fontSize: 24,
        shadowColor: 'black',
    },
    scrollListTags: {
        fontSize: 18,
        shadowColor: 'black',
        marginTop: width / 15
    },
    viewWrapper: {
        margin: 50,
        flex: 1,
        fontWeight: 'bold',
    },
    safeAreaViewWrapper: {
        marginTop: width / 10,
        backgroundColor: '#B6D349',
    },
    textTitle: {
        marginTop: width / 10,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    subViewStyle: {
        marginTop: width / 10,
        marginLeft: 20
    }

});

export default Gratitude;