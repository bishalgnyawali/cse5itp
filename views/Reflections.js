import React from "react";
import {SafeAreaView,StyleSheet, TouchableOpacity} from "react-native";
import Reflection from "../components/reflection/ReflectionItem";
import DefaultStyles from "../assets/Style";
import Header from "../components/common/Header";
import {reflections} from "../objects/Reflections";
import Icon from "react-native-vector-icons/Feather"
import { ScrollView } from "react-native-gesture-handler";

export default Reflections = (props) => {
    return (
        <SafeAreaView style={[DefaultStyles.container, DefaultStyles.green]}>
            <Header navigation={props.navigation} mainTitle="Reflection"></Header>
            <ScrollView style={DefaultStyles.viewWrapper}>
                {reflections.map((reflection, index) => 
                    <Reflection key={index} date={reflection.date} mood={reflection.mood} activities={reflection.activties} />
                )}
            </ScrollView>
            <TouchableOpacity onPress={() => props.navigation.navigate('ReflectMood')} style={[DefaultStyles.teal,styles.floatingButton]}>
                <Icon name="plus" style={{fontSize:30, color:'#fff'}} />
            </TouchableOpacity>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    floatingButton: {
        borderRadius: 50,
        width: 60,
        height: 60,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset: {width:4,height:4},
        shadowColor: '#111',
        shadowOpacity: 0.25,
        position:'absolute',
        bottom: 50,
        right: 50,
        zIndex: 2
    }
});