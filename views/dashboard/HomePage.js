import React from 'react'
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    Image,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

const { width, height } = Dimensions.get('window');

class HomePage extends React.Component {

    render() {
        return (
            <SafeAreaView style={styles.safeAreaViewWrapper}>
                <Feather 
                    onPress= {()=> this.props.navigation.openDrawer()}
                    name="menu" color="black" style={{ alignSelf: 'flex-start', marginLeft: 20, marginTop: 20 }} size={30} />

                <View style= {{width, marginTop: 30, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center'}}>

                <TouchableOpacity 
                        onPress= {()=> this.props.navigation.navigate('TopTwo')}
                        style= {{width, padding: 16, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <View style= {{width: '80%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                            <Image source = {require('../../assets/images/myValues.png')} style= {{width: width / 10, height: width / 10}} resizeMode= "contain" />

                            <View style= {{ flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center'}}>
                                <Text style= {styles.textLink}>My Values</Text>
                                <Text style= {styles.subTextLink}>What makes me good?</Text>
                            </View>
                        </View>
                        <Feather name="chevron-right" size= {50} color= "#00A6FF" style= {{}} />
                </TouchableOpacity>

                    <TouchableOpacity 
                        onPress= {()=> this.props.navigation.navigate('Gratitude')}
                        style= {{width, padding: 16, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <View style= {{width: '80%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                            <Image source = {require('../../assets/images/grateful.png')} style= {{width: width / 10, height: width / 10}} resizeMode= "contain" />

                            <View style= {{ flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center'}}>
                                <Text style= {styles.textLink}>Grateful</Text>
                                <Text style= {styles.subTextLink}>What am I grateful for?</Text>
                            </View>
                        </View>
                        <Feather name="chevron-right" size= {50} color= "#00A6FF" style= {{}} />
                    </TouchableOpacity>
                    <TouchableOpacity 
                            onPress= {()=> this.props.navigation.navigate('Reflections')}
                            style= {{width, padding: 16, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        
                            <View style= {{width: '80%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                                <Image source = {require('../../assets/images/reflection.png')} style= {{width: width / 10, height: width / 10}} resizeMode= "contain" />

                                <View style= {{ flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center'}}>
                                    <Text style= {styles.textLink}>Reflections</Text>
                                    <Text style= {styles.subTextLink}>How am I tracking?</Text>
                                </View>
                            </View>
                            <Feather name="chevron-right" size= {50} color= "#00A6FF" style= {{}} />
                    </TouchableOpacity>

                    <TouchableOpacity 
                        onPress= {()=> this.props.navigation.navigate('MindFulness')}
                        style= {{width, padding: 16, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <View style= {{width: '80%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                            <Image source = {require('../../assets/images/mindfullness.png')} style= {{width: width / 10, height: width / 10}} resizeMode= "contain" />

                            <View style= {{ flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center'}}>
                                <Text style= {styles.textLink}>Mindfulness</Text>
                                <Text style= {styles.subTextLink}></Text>
                            </View>
                        </View>
                        <Feather name="chevron-right" size= {50} color= "#00A6FF" style= {{}} />
                    </TouchableOpacity>

                </View>
                


            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    iconWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 50,
        marginBottom: 50,
    },
    text: {
        fontWeight: 'bold',
        shadowColor: 'black',
        marginTop: 8,
    },
    viewWrapper: {
        margin: 50,
        flex: 1,
        fontWeight: 'bold',
    },
    safeAreaViewWrapper: {
        flex: 1,
        backgroundColor: '#B6D349',
    },
    textLink: {
        fontSize: 24,
        textAlign: 'left',
        marginLeft: 6,
        width: '100%'
    },
    subTextLink: {
        fontSize: 14,
        color: '#00A6FF',
        textAlign: 'left',
        marginLeft: 4,
        width: '100%'
    },
    imageWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
    }
});

export default HomePage;
