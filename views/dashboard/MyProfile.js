import { SafeAreaView,View, Text} from 'react-native'
import Button from '../../components/common/Button';
import DefaultStyles from '../../assets/Style';
import Feather from 'react-native-vector-icons/Feather';
import React from 'react';
export default MyProfile = (props) => {
    
    return (
        <SafeAreaView style={[DefaultStyles.green,{flex:1}]}>
            <Feather 
                    onPress= {()=> props.navigation.openDrawer()}
                    name="menu" color="black" style={{ alignSelf: 'flex-start', marginLeft: 20, marginTop: 20 }} size={30} />
                <View style={{margin:20}}>
                        <Text>MyProfile</Text>
                        <Button onPress={()=>{props.navigation.navigate('HomePage')}} text={'Done'}/>
                </View>
        </SafeAreaView>);
}