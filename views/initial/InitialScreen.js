import React,{useState,useEffect} from 'react'
import { View,SafeAreaView, Image, StyleSheet,Dimensions } from 'react-native'
import Header from '../../components/common/Header';
import Button from '../../components/common/Button';
import DefaultStyles from '../../assets/Style';
import AsyncStorage from '@react-native-community/async-storage';
//import HomePage from './views/dashboard/HomePage';

const InitialScreen = (props) => {
    const [check,setCheck]=useState(false);

          useEffect(()=>{
                const cal = async ()=>{
                res =  await AsyncStorage.getItem('USER_KEY');
                if(res==='true')
                    setCheck(true);
                    console.log(res);
                }
                cal();
                return function cleanup() {
                    setCheck(false);
                };
          });

        if(!check){
        return (
            <SafeAreaView style={[DefaultStyles.teal, DefaultStyles.container]}>
                <View style={styles.headerWrapper}>
                    <Image style={DefaultStyles.logo} source={require('../../assets/images/logo.png')}/>
                    <Header mainTitle="Mentally Fit"></Header>
                </View>
                <Image source={require('../../assets/images/MentalHealth-GettyImages.jpg')} 
                    style={styles.imageWrapper}
                />
                <Button onPress = {() => props.navigation.navigate('Register') } color={'green'} text={'Sign Up'} />
                <Button onPress = {() => props.navigation.navigate('LogIn') } color={'white'} text={'I Have an Account'} />
            </SafeAreaView>
        )
        }
        else{
            return(props.navigation.navigate('HomePage'))
        }
    }


export default InitialScreen;

const styles=StyleSheet.create({
    headerWrapper:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    imageWrapper:{
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height*0.5963,//373.33
        resizeMode:'contain'
    }
});