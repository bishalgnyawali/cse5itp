import React from 'react'
import { View, Text, SafeAreaView, Image, StyleSheet, Dimensions } from 'react-native'
import Header from '../../components/common/Header';
import Button from '../../components/common/Button';
import DefaultStyles from '../../assets/Style';

const LogIn = (props) => {
    //console.log(Dimensions.get('window').width+" "+Dimensions.get('window').height);
    return (
        <SafeAreaView style={[DefaultStyles.green, DefaultStyles.container]}>
            <Text>Should be a login page</Text>
            <Button onPress = {() => props.navigation.navigate('MindFulness') } color={'white'} text={'Login'} />
        </SafeAreaView>
    )
}

export default LogIn

const styles=StyleSheet.create({
    headerWrapper:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    imageWrapper:{
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height*0.5963,//373.33
        resizeMode:'contain'
    }
});