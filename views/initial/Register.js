import React from 'react'
import { KeyboardAvoidingView,Platform,StyleSheet } from 'react-native'
import RegistrationForm from '../../components/RegistrationForm';

const Register = (props) => {
    return (
        <KeyboardAvoidingView
            behavior={(Platform.OS === 'ios')? "padding" : null}//behavior="padding" 
            style={styles.container}
            //keyboardVerticalOffset={Constants.statusBarHeight}
            enabled
            >
            <RegistrationForm navigation={props.navigation}/>
        </KeyboardAvoidingView>
    )
}

export default Register;

const styles=StyleSheet.create({
    container: {
        //paddingTop: 2,
        flex:1,
        //alignItems:'center',
        backgroundColor:'#0094A2',
        margin:0,
    }
})