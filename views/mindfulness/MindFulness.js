import React,{useEffect} from 'react'
import { 
    View,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    
} from 'react-native'
import Header from '../../components/common/Header';
//import Button from '../../components/common/Button';
import VideoPlayer from 'react-native-video-controls';
import VideoSpecific from './VideoSpecific';
import DefaultStyles from '../../assets/Style';
import Feather from 'react-native-vector-icons/Feather'
const MindFulness = (props) => {
    
    const url='https://vjs.zencdn.net/v/oceans.mp4' ;
    
    return (
    <SafeAreaView style={[styles.container, DefaultStyles.green]}>
    <Feather 
            onPress= {()=> props.navigation.openDrawer()}
            name="menu" color="black" style={{ alignSelf: 'flex-start', marginLeft: 20, marginTop: 20 }} size={30} 
        />
        <Header style={styles.headerWrapper} mainTitle="Mindfulness"/>
        <ScrollView>
            <View style={{marginTop:0, height:300}}>
                    <VideoPlayer
                        source={require('../../assets/videos/sample.mp4')}
                        paused={true}
                        title={'Deep Breathe'}
                        showOnStart={false}
                        toggleResizeModeOnFullscreen={false}
                        controlTimeout={0}
                        hideShutterView={true}
                        disablePlayPause={true}
                        disableFullscreen={true}
                        disableVolume={true}
                        disableSeekbar={true}
                        disableBack={true}
                        //playInBackground={true}
                        //navigator={ this.props.navigator }
                    />
                
            </View>
            <View style={{marginTop:30, height:300}}>
                <VideoPlayer
                    source={require('../../assets/videos/sample.mp4')}
                    paused={true}
                    showOnStart={false}
                    onPlay={()=>{return <VideoSpecific/>}}
                    //playInBackground={true}
                    //navigator={ this.props.navigator }
                />
            </View>
            <View style={{marginTop:30, height:300}}>
                <VideoPlayer
                    source={require('../../assets/videos/sample.mp4')}
                    paused={true}
                    showOnStart={false}
                    //playInBackground={true}
                    //navigator={ this.props.navigator }
                />
            </View>
            <View style={{marginTop:30, height:300}}>
                <VideoPlayer
                    source={require('../../assets/videos/sample.mp4')}
                    paused={true}
                    showOnStart={false}
                    //playInBackground={true}
                    //navigator={ this.props.navigator }
                />
            </View>
            <View style={{marginTop:30,marginBottom:70, height:300}}>
                <VideoPlayer
                    source={require('../../assets/videos/sample.mp4')}
                    paused={true}
                    title={'Deep Breathe'}
                    showOnStart={false}
                    toggleResizeModeOnFullscreen={false}
                    controlTimeout={0}
                    hideShutterView={true}
                    disablePlayPause={true}
                    disableFullscreen={true}
                    disableVolume={true}
                    disableSeekbar={true}
                    disableBack={true}
                    //playInBackground={true}
                    // navigator={ this.props.navigator }
                />
            </View>
        </ScrollView>
    </SafeAreaView>  
    );
}

export default MindFulness;

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    headerWrapper:{
        //marginBottom:20,
        alignSelf:'center'
    },
})