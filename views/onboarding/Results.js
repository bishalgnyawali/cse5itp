import React from 'react'
import {SafeAreaView,View,Text,StyleSheet} from 'react-native'
import DefaultStyles from '../../assets/Style';
import Button from '../../components/common/Button';
import Header from '../../components/common/Header';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default Results = (props) => {
    var result=props.navigation.state.params.result;
    var listSorted=[...result]
    listSorted.sort(function(a, b){return a - b}).reverse();
    const List = ['Love and Belonging','Power','Freedom','Fun','Survival'];
    
    //console.warn(listSorted);
    //console.warn("res"+result)
    
    return (
        <SafeAreaView style={[styles.container, DefaultStyles.green]}>
            <Header style={styles.headerWrapper} mainTitle="Your Results"/>
            <View style={{margin:20}}>
                {   
                    listSorted.map((item,key)=>{
                        var i=result.indexOf(item)
                        result[i]=key;
                        //console.warn(result);
                        return(
                            <View style={styles.listContainer}>
                                <Text style={styles.textStyle} key={key}>{List[i]}    (value={listSorted[key]})</Text>
                            </View>
                        )
                    })
                }
                
            </View>
            <Button onPress={()=>{props.navigation.navigate('TopTwo')}} text={'Next'}/>
        </SafeAreaView>
    );
}

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    headerWrapper:{
        //marginBottom:20,
        alignSelf:'center',
        justifyContent:'space-around'
    },
    listContainer:{
        margin:5,
        backgroundColor:'#616f39',
        shadowColor:'black',
        shadowOpacity:0.7,
        width:wp('84.5%'),
        height:hp('12%'),
        borderColor:'black'
    },
    textStyle:{
        alignContent:'center',
        alignSelf:'center',
        fontSize:20,
        fontWeight:'bold',
        margin:25,
    }
})






