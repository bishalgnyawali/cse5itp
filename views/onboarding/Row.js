import React, { useEffect } from 'react';
import {
    Animated,
    Easing,
    StyleSheet,
    Text,
    Image,
    Dimensions,
    Platform,
    //BackHandler
} from 'react-native';


const window = Dimensions.get('window');

export default Row =(props)=> {
    
    _active = new Animated.Value(0);

    _style = {
        ...Platform.select({
        ios: {
            transform: [{
                scale: _active.interpolate({
                inputRange: [0, 1],
                outputRange: [1, 1.1],
                }),
        }],
        shadowRadius: _active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 10],
        }),
        },

        android: {
        transform: [{
            scale: _active.interpolate({
                inputRange: [0, 1],
                outputRange: [1, 1.07],
            }),
        }],
        elevation: _active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 6],
        }),
        },
    })
    };
  
useEffect(() => {
    Animated.timing(_active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(props.active),
    }).start();
}, [props.active])

  
const {data, active} = props;

    return (
        <Animated.View style={[
            styles.row,
            _style,
        ]}>
            <Image source={{uri: data.image}} style={styles.image} />
            <Text style={styles.text}>{data.text}</Text>
        </Animated.View>
    );
}



const styles = StyleSheet.create({
    
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#B9DD4B',
        padding: 16,
        height: 80,
        flex: 1,
        marginTop: 7,
        marginBottom: 12,
        borderRadius: 4,
    
    
        ...Platform.select({
            ios: {
                width: window.width - 30 * 2,
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOpacity: 1,
                shadowOffset: {height: 2, width: 2},
                shadowRadius: 2,
            },
    
            android: {
                width: window.width - 30 * 2,
                elevation: 0,
                marginHorizontal: 30,
            },
        })
    },
    
    image: {
        width: 50,
        height: 50,
        marginRight: 25,
        borderRadius: 25,
    },
    
    text: {
        fontSize: 15,
        color: '#222222',
    },
    });
    