import React from 'react'
import { SafeAreaView,View, Text,StyleSheet} from 'react-native'
import Button from '../../components/common/Button';
import Header from '../../components/common/Header';
import DefaultStyles from '../../assets/Style';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {onSignIn} from '../../objects/auth';
export default TopTwo = (props) => {
    //AsyncStorage.setItem('logIn', 'true')
    onSignIn();
    return (
        <SafeAreaView style={[DefaultStyles.green,{flex:1}]}>
                <Header style={styles.headerWrapper} mainTitle="Your Top Two"/>
                <View style={{margin:20}}>
                        <View style={styles.listContainer}>
                                <Text style={styles.textStyle}>Hello Top One</Text>
                        </View>
                        <View style={styles.listContainer}>
                                <Text style={styles.textStyle}>Hello Top Two</Text>
                        </View>
                        <Button onPress={()=>{props.navigation.navigate('HomePage')}} text={'Done'}/>
                </View>
        </SafeAreaView>);
}
const styles=StyleSheet.create({
        listContainer:{
                margin:5,
                backgroundColor:'#a7d129',
                shadowColor:'black',
                shadowOpacity:0.7,
                width:wp('84.5%'),
                height:hp('12%'),
                borderColor:'black'
        },
        textStyle:{
                alignContent:'center',
                alignSelf:'center',
                fontSize:20,
                fontWeight:'bold',
                margin:25,
        }
})