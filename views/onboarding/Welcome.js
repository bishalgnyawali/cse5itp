import React,{useState} from 'react'
import { View,SafeAreaView,Dimensions,StyleSheet } from 'react-native'
import Header from '../../components/common/Header';
import Button from '../../components/common/Button';
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation';
import DefaultStyles from '../../assets/Style';
const Welcome = (props) => {
    /* useEffect(()=>{
       // function backButtonHandler() {return true}
        //props.navigation.pop();
        //props.navigation.pop();
        BackHandler.addEventListener("hardwareBackPress", ()=>{return true});
        return function cleanup() {
            BackHandler.removeEventListener("hardwareBackPress", ()=>{return false});
        };
    },[]); */
    const heightW=Dimensions.get('window').height;
    const widthW=Dimensions.get('window').width;
    const url='https://vjs.zencdn.net/v/oceans.mp4';
    var [portrait,setPortrait]=useState(true);

    const Elem=()=>{

        if(portrait){
        return(
            <SafeAreaView style={[styles.container, DefaultStyles.green]}> 
                <Header style={{padding:20,alignSelf:'center'}} mainTitle="Welcome to Mentally Fit Ep"/> 
                <View style={{height:300,marginBottom:60}}>
                
                    <VideoPlayer
                        source={require('../../assets/videos/sample.mp4')}
                        paused={true}
                        onEnterFullscreen={()=>{
                                setPortrait(!portrait);
                                Orientation.lockToLandscape()
                            }}
                        toggleResizeModeOnFullscreen={true}
                        style={styles.videoWrapper}
                        fullscreen={true}
                    />
                    
                </View>
                <Button onPress={()=>props.navigation.navigate('SortableValues')} text={"Next"}/>
            </SafeAreaView>
        )
        }

        else{

            return(
                <View> 
        
                    <View style={{height:widthW}}>
                        {/* <Header style={{padding:20,alignSelf:'center'}} mainTitle="Welcome to Mentally Fit Ep"/>   */}  
                        <VideoPlayer
                            source={require('../../assets/videos/sample.mp4')}
                            paused={true}
                            onEnterFullscreen={()=>{
                                setPortrait(!portrait);
                                Orientation.lockToPortrait()
                                
                            }}
                            // onExitFullscreen={()=>{
                            //         setPortrait(!portrait);
                            //         Orientation.lockToPortrait()
                            //     }}
                            //playInBackground={true}
                            //navigator={ this.props.navigator }
                            toggleResizeModeOnFullscreen={true}
                            style={[styles.videoWrapper,{height:!portrait?widthW:null}]}
                            fullscreen={true}
                            resizemode={'contain'}
                            hideShutterView={true}
                        />
                    </View>
                    
                </View>
            )

        }
    }
    return (
        <>
            <Elem/>
        </> 
    )
}

export default Welcome;

const styles=StyleSheet.create({
    container:{
        flex:1,
        
    },
    videoWrapper:{
        position:'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        marginHorizontal:0,
    }
    //(portrait)?{height:300}:{height:700},
    //     width:Dimensions.get('window').width,
    //     //width:Dimensions.get('window').width,
    // }
});
